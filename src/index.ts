/**
 * Provides the route file of the module exposing all the relevant classes.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import {
    AutoConverters, AutoConverter,
    BooleanConverter, DateConverter, NumberConverter,
    RegexCaseInsensitiveConverter, RegexConverter, StringConverter,
} from "./Converters";
import ParseParamTokens from "./Lib/ParseParamTokens";
import {
    ComparisonOperators, Eq, Fields, From, Gt, Gte, In, Limit, LogicalOperators, Lt, Lte, ModifierOperators,
    Ne, Nin, Sort,
} from "./Operators";
import Parser, { IQuery, TParams } from "./Parser";

export default Parser;
export {
    // Lib utils.
    TParams,
    IQuery,
    ParseParamTokens,

    // Type converters.
    AutoConverter, AutoConverters,
    BooleanConverter,
    DateConverter,
    NumberConverter,
    RegexConverter, RegexCaseInsensitiveConverter,
    StringConverter,

    // Logical operators.
    LogicalOperators,

    // Comparison operators.
    ComparisonOperators,
    Eq, Gt, Gte, In, Lt, Lte, Ne, Nin,

    // Modifier operators.
    ModifierOperators,
    Fields, From, Limit, Sort,
};
