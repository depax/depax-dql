/**
 * Provides a number converter.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

/**
 * Converts the given value in to a number value.
 *
 * @param value The value to convert.
 *
 * @return Returns the converted value.
 */
export default function NumberConverter(value: any): number {
    const n = +value;
    if (isNaN(n)) {
        return 0;
    }

    return n;
}
