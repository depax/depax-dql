/**
 * Provides a boolean converter.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

/**
 * Converts the given value in to a boolean value.
 *
 * @param value The value to convert.
 *
 * @return Returns the converted value.
 */
export default function BooleanConverter(value: any): boolean {
    return [
        "true", "yes", 1, "1", "on",
    ].indexOf(value) > -1;
}
