/**
 * Provides an index of all defined converter callbacks.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import AutoConverter, { AutoConverters } from "./AutoConverter";
import BooleanConverter from "./BooleanConverter";
import DateConverter from "./DateConverter";
import NumberConverter from "./NumberConverter";
import RegexConverter, { RegexCaseInsensitiveConverter } from "./RegexConverter";
import StringConverter from "./StringConverter";

/**
 * A collection of defined converters.
 */
const Converters: {
    [name: string]: (value: any) => any;
} = {
    auto: AutoConverter,
    bool: BooleanConverter,
    boolean: BooleanConverter,
    date: DateConverter,
    number: NumberConverter,
    re: RegexConverter,
    regex: RegexConverter,
    regexi: RegexCaseInsensitiveConverter,
    rei: RegexCaseInsensitiveConverter,
    string: StringConverter,
};

export default Converters;
export {
    AutoConverter, AutoConverters,
    BooleanConverter,
    DateConverter,
    NumberConverter,
    RegexConverter, RegexCaseInsensitiveConverter,
    StringConverter,
};
