/**
 * Provides a string converter.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

/**
 * Converts the given value in to a decoded string value.
 *
 * @param value The value to convert.
 *
 * @return Returns the converted value.
 */
export default function StringConverter(value: any): string {
    return decodeURIComponent(value);
}
