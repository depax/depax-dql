/**
 * Provide's converters for the basic values.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

/**
 * Provide some standard auto convert values.
 */
export const AutoConverters = {
    "-Infinity": -Infinity,
    "Infinity": Infinity,
    "false": false,
    "null": null,
    "true": true,
    "undefined": undefined,
};

/**
 * Attempt to automatically convert a given value.
 *
 * @param value The value to attempt to convert.
 *
 * @return Returns the converted value.
 */
export default function AutoConverter(value: any): any {
    if (AutoConverters.hasOwnProperty(value)) {
        return AutoConverters[value];
    }

    const n = +value;
    if (isNaN(n) || n.toString() !== value) {
        return decodeURIComponent(value);
    }

    return n;
}
