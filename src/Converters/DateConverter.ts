/**
 * Provides a date converter.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

/**
 * Converts the given value in to a date value.
 *
 * @param value The value to convert.
 *
 * @return Returns the converted value.
 */
export default function DateConverter(value: any): Date {
    let date;
    if (!isNaN(value)) {
        date = new Date(value * 1000);
    } else {
        date = new Date(value);
    }

    if (!date || isNaN(date.getTime())) {
        date = new Date(0);
    }

    return date;
}
