/**
 * Provides a RexExp converters.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import StringConverter from "./StringConverter";

/**
 * Converts the given value in to a RegExp, without any flags defined.
 *
 * @param value The value to convert.
 *
 * @return Returns the converted value.
 */
export default function RegexConverter(value: any): RegExp {
    const pattern = StringConverter(value);
    return new RegExp(pattern);
}

/**
 * Converts the given value in to a RegExp, and applying the "i" flag.
 *
 * @param value The value to convert.
 *
 * @return Returns the converted value.
 */
export function RegexCaseInsensitiveConverter(value: any): RegExp {
    const pattern = StringConverter(value);
    return new RegExp(pattern, "i");
}
