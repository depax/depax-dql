/**
 * The main code to parse the given query string.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import {
    AutoConverter,
    BooleanConverter,
    DateConverter,
    NumberConverter,
    RegexCaseInsensitiveConverter,
    RegexConverter,
    StringConverter,
} from "./Converters";
import Tokenize from "./Lib/Tokenize";
import { ComparisonOperators, LogicalOperators, ModifierOperators } from "./Operators";

/**
 * A type to define the expected params.
 */
export type TParams = any[];

/**
 * A simple interface to describe the output of the parser.
 */
export interface IQuery {
    /** Any errors raised while parsing. */
    errors?: string[];

    /** Which table/collection is being queried. */
    from?: string;

    /** The fields to return of the query. */
    fields?: string[];

    /** How to sort the query results. */
    sort?: { [field: string]: boolean };

    /** Define the limit of offset of the query. */
    limit?: { limit: number, offset: number };

    /** The condition selectors for the query. */
    selectors?: { [field: string]: any };
}

/**
 * This function collected the tokenized version of the given query string and parses it.
 *
 * @param queryString The query string to tokenize and parse.
 * @param params Any parameters to use.
 *
 * @return Returns a query object.
 */
export default function Parser(queryString: string, params: TParams = []): IQuery {
    const query: IQuery = {};
    const tokenized = Tokenize(queryString, params);

    function convert(operator, parent?: any) {
        try {
            if (LogicalOperators[operator[0]]) {
                // Check for logical operators.
                if (!query.selectors) {
                    query.selectors = {};
                }

                if (parent) {
                    // If a parent is defined, it means this has been called from within a logical operator.
                    parent[LogicalOperators[operator[0]]] = {};
                    operator[1].forEach((op) => convert(op, parent[LogicalOperators[operator[0]]]));
                } else {
                    // No parent, so we add the data to the selectors of the query.
                    query.selectors[LogicalOperators[operator[0]]] = {};
                    operator[1].forEach((op) => convert(op, query.selectors[LogicalOperators[operator[0]]]));
                }
            } else if (ComparisonOperators[operator[0]]) {
                // Check for comparison operators.
                if (!query.selectors) {
                    query.selectors = {};
                }

                // Execute the comparison callback, and extract the field and value, and add to the query object.
                const [ field, value ] = ComparisonOperators[operator[0]](operator[1], params);
                if (parent) {
                    parent[field] = value;
                } else {
                    query.selectors[field] = value;
                }
            } else if (ModifierOperators[operator[0]]) {
                // Check for modifier operators.
                ModifierOperators[operator[0]](operator[1], params, query);
            } else {
                // We have been unable to find a matching operator.
                throw new Error(`The unknown operator "${operator[0]}" has been identified.`);
            }
        } catch (err) {
            // Capture any errors and add to the queries error array.
            if (!query.errors) {
                query.errors = [];
            }

            query.errors.push(err.message);
        }
    }

    // Do a quick check that we have a matching number of opening and closing parentheses.
    const openParentheses = (queryString.match(/\(/g) || []).length;
    const closingParentheses = (queryString.match(/\)/g) || []).length;
    if (openParentheses !== closingParentheses) {
        query.errors = [ "Opening and closing parentheses mismatch." ];
    } else {
        // No issues, so convert the tokenized query.
        (tokenized as any[]).forEach((token) => convert(token));
    }

    return query;
}
