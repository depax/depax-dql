/**
 * Provides an index of all defined operator callbacks.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import { IQuery, TParams } from "../Parser";
import Eq from "./Comparison/Eq";
import Gt from "./Comparison/Gt";
import Gte from "./Comparison/Gte";
import In from "./Comparison/In";
import Lt from "./Comparison/Lt";
import Lte from "./Comparison/Lte";
import Ne from "./Comparison/Ne";
import Nin from "./Comparison/Nin";
import Fields from "./Modifiers/Fields";
import From from "./Modifiers/From";
import Limit from "./Modifiers/Limit";
import Sort from "./Modifiers/Sort";

/**
 * A collection of defined comparison operators.
 */
export const ComparisonOperators: {
    [op: string]: (args: string, params: TParams) => any;
} = {
    eq: Eq,
    gt: Gt,
    gte: Gte,
    in: In,
    lt: Lt,
    lte: Lte,
    ne: Ne,
    nin: Nin,
};

/**
 * A collection of defined logical operators.
 */
export const LogicalOperators: {
    [op: string]: string;
} = {
    and: "$and",
    nor: "$nor",
    not: "$not",
    or: "$or",
};

/**
 * A collection of defined modifier operators.
 */
export const ModifierOperators: {
    [op: string]: (args: string, params: TParams, query: IQuery) => void;
} = {
    fields: Fields,
    from: From,
    limit: Limit,
    sort: Sort,
};

export default { ComparisonOperators, LogicalOperators, ModifierOperators };
export {
    // Comparison operators.
    Eq, Gt, Gte, In, Lt, Lte, Ne, Nin,

    // Modifier operators.
    Fields, From, Limit, Sort,
};
