/**
 * Provides the "Fields" modifier operator.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import ParseParamTokens from "../../Lib/ParseParamTokens";
import { IQuery } from "../../Parser";

/**
 * Convert the given string args into valid arguments for the modifier operator.
 *
 * @param args The string arguments from the query string.
 * @param params The parameters sent with the query string for token replacements.
 *
 * @return Returns an array with the field name and operator value.
 */
export default function ModifierOperatorFields(args: string, params: any, query: IQuery): void {
    if (!query.fields) {
        query.fields = [];
    }

    args.split(",").forEach((field: string) => query.fields.push(ParseParamTokens(field, params)));
}
