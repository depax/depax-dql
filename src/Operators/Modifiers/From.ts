/**
 * Provides the "From" modifier operator.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import ParseParamTokens from "../../Lib/ParseParamTokens";
import { IQuery } from "../../Parser";

/**
 * Convert the given string args into valid arguments for the modifier operator.
 *
 * @param args The string arguments from the query string.
 * @param params The parameters sent with the query string for token replacements.
 *
 * @return Returns a string value representing the table/collection name.
 */
export default function ModifierOperatorFrom(args: string, params: any, query: IQuery): void {
    query.from = ParseParamTokens(args.split(",")[0], params);
}
