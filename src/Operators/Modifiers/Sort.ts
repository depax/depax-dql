/**
 * Provides the "Sort" modifier operator.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import ParseParamTokens from "../../Lib/ParseParamTokens";
import { IQuery } from "../../Parser";

/**
 * Convert the given string args into valid arguments for the modifier operator.
 *
 * @param args The string arguments from the query string.
 * @param params The parameters sent with the query string for token replacements.
 *
 * @return Returns an object containing each of the fields and a boolean value to determine the sort direction,
 *   true (+) for ascending and false (-) for descending.
 */
export default function ModifierOperatorSort(args: string, params: any, query: IQuery): void {
    if (!query.sort) {
        query.sort = {};
    }

    args.split(",").forEach((field: string) => {
        field = ParseParamTokens(field, params);

        let direction = true;
        if ([ "+", "-" ].indexOf(field[0]) > -1) {
            direction = (field[0] === "+");
            field = field.substr(1);
        }

        query.sort[field] = direction;
    });
}
