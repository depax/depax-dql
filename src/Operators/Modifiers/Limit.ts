/**
 * Provides the "Limit" modifier operator.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import ParseParamTokens from "../../Lib/ParseParamTokens";
import { IQuery } from "../../Parser";

/**
 * Convert the given string args into valid arguments for the modifier operator.
 *
 * @param args The string arguments from the query string.
 * @param params The parameters sent with the query string for token replacements.
 *
 * @return Returns an array containing the limit and offset values.
 */
export default function ModifierOperatorLimit(args: string, params: any, query: IQuery): void {
    const [ limit, offset ] = args.split(",");

    query.limit = {
        limit: Math.max(0, parseInt(ParseParamTokens(limit || "0", params), 10)),
        offset: Math.max(0, parseInt(ParseParamTokens(offset || "0", params), 10)),
    };
}
