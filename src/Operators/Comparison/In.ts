/**
 * Provides the "In" comparison operator.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import ParseParamTokens from "../../Lib/ParseParamTokens";
import { TParams } from "../../Parser";

/**
 * Convert the given string args into valid arguments for the comparison operator.
 *
 * @param args The string arguments from the query string.
 * @param params The parameters sent with the query string for token replacements.
 *
 * @return Returns an array with the field name and operator value.
 */
export default function ComparisonOperatorIn(args: string, params: TParams): any[] {
    const splitArgs = args.split(",");

    const field = ParseParamTokens(splitArgs.shift(), params);
    let values = [];

    splitArgs.forEach((value, idx) => {
        const v = ParseParamTokens(value, params);
        if (v instanceof Array) {
            values = [].concat(values, v);
        } else {
            values.push(v);
        }
    });

    return [ field, { $in: values } ];
}
