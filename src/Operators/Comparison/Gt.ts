/**
 * Provides the "Gt" comparison operator.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import ParseParamTokens from "../../Lib/ParseParamTokens";
import { TParams } from "../../Parser";

/**
 * Convert the given string args into valid arguments for the comparison operator.
 *
 * @param args The string arguments from the query string.
 * @param params The parameters sent with the query string for token replacements.
 *
 * @return Returns an array with the field name and operator value.
 */
export default function ComparisonOperatorGt(args: string, params: TParams): any[] {
    let [ field, value ] = args.split(",");
    field = ParseParamTokens(field, params);
    value = ParseParamTokens(value, params);

    return [ field, { $gt: value } ];
}

