/**
 * Provides the "Nin" comparison operator.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import { TParams } from "../../Parser";
import ComparisonOperatorIn from "./In";

/**
 * Convert the given string args into valid arguments for the comparison operator.
 *
 * @param args The string arguments from the query string.
 * @param params The parameters sent with the query string for token replacements.
 *
 * @return Returns an array with the field name and operator value.
 */
export default function ComparisonOperatorNin(args: string, params: TParams): any[] {
    const comp = ComparisonOperatorIn(args, params);

    return [ comp[0], { $nin: comp[1].$in } ];
}

