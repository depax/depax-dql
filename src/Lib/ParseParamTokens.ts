/**
 * A utility function to handle parsing tokens and converting value types.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import Converters from "../Converters";
import { TParams } from "../Parser";

/**
 * Parse a given value, check if its a valid token, and attempt to covnert it.
 *
 * @param value The value to parse and convert.
 * @param params The parameters passed to the parser.
 *
 * @return Returns the parsed and converted value.
 */
export default function ParseParamTokens(value: string, params: TParams): any {
    let converter = "auto";
    if (value.charAt(0) === "$") {
        const idx = parseInt(value.substring(1), 10) - 1;
        return idx >= 0 && params ? params[idx] : undefined;
    } else if (value.indexOf(":") > -1) {
        const parts = value.split(":");

        converter = parts[0];
        value = parts.slice(1).join(":").toLowerCase();

        if (!Converters[converter]) {
            throw new Error(`The unknown converter "${converter}" has been identified.`);
        }

        return Converters[converter](value);
    }

    return Converters.auto(value);
}
