/**
 * Provides a util function to explode a string out into tokenized elements.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import { TParams } from "../Parser";
import ParseParamTokens from "./ParseParamTokens";

/**
 * Tokenize a given query string.
 *
 * @param value The query string to tokenize.
 * @param params The params from the parser.
 *
 * @return Returns tokens or a string match if it cant be tokenized.
 */
export default function Tokenize(value: string, params: TParams): any[] | string {
    const leftRx = new RegExp(/[a-z\$]*\w\(|\)/, "gi");
    const rightRx = new RegExp(/\(/, "i");

    const extracted = [];
    let operator = [];

    let i = 0;
    let lastIndex;
    let match;

    do {
        i = 0;

        match = leftRx.exec(value);
        while (match) {
            if (rightRx.test(match[0])) {
                if (!i++) {
                    lastIndex = leftRx.lastIndex;
                }

                // If this is the first iteration, then it contains the operator name.
                if (i === 1) {
                    operator = [ ParseParamTokens(match[0].slice(0, -1).toLowerCase(), params) ];
                }
            } else if (i && !--i) {
                // Add the operator arguments to the array.
                operator.push(Tokenize(value.slice(lastIndex, match.index), params));

                // Add the operator to the extracted results.
                extracted.push(operator);
            }

            match = leftRx.exec(value);
        }
    } while (i && leftRx.lastIndex === lastIndex);

    return extracted.length > 0 ? extracted : value;
}
