@logical-operators @and
Feature: Provide tests for the AND logical operator

  Scenario: Should parse the logical operator
    Given I parse the query:
      """
      and(eq(a,b),eq(b,c),eq(c,d))
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "$and": {
            "a": { "$eq": "b" },
            "b": { "$eq": "c" },
            "c": { "$eq": "d" }
          }
        }
      }
      """

  Scenario: Should parse the logical operator and bind param
    Given I parse the query with params:
      """
      {
        "query": "and(eq(a,$1),eq($1,c),eq(c,d))",
        "params": [ "b" ]
      }
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "$and": {
            "a": { "$eq": "b" },
            "b": { "$eq": "c" },
            "c": { "$eq": "d" }
          }
        }
      }
      """
