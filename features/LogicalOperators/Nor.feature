@logical-operators @nor
Feature: Provide tests for the NOR logical operator

  Scenario: Should parse the logical operator
    Given I parse the query:
      """
      nor(eq(a,b),eq(b,c),eq(c,d))
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "$nor": {
            "a": { "$eq": "b" },
            "b": { "$eq": "c" },
            "c": { "$eq": "d" }
          }
        }
      }
      """

  Scenario: Should parse the logical operator and bind param
    Given I parse the query with params:
      """
      {
        "query": "nor(eq(a,$1),eq($1,c),eq(c,d))",
        "params": [ "b" ]
      }
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "$nor": {
            "a": { "$eq": "b" },
            "b": { "$eq": "c" },
            "c": { "$eq": "d" }
          }
        }
      }
      """
