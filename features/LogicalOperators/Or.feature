@logical-operators @or
Feature: Provide tests for the OR logical operator

  Scenario: Should parse the logical operator
    Given I parse the query:
      """
      or(eq(a,b),eq(b,c),eq(c,d))
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "$or": {
            "a": { "$eq": "b" },
            "b": { "$eq": "c" },
            "c": { "$eq": "d" }
          }
        }
      }
      """

  Scenario: Should parse the logical operator and bind param
    Given I parse the query with params:
      """
      {
        "query": "or(eq(a,$1),eq($1,c),eq(c,d))",
        "params": [ "b" ]
      }
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "$or": {
            "a": { "$eq": "b" },
            "b": { "$eq": "c" },
            "c": { "$eq": "d" }
          }
        }
      }
      """
