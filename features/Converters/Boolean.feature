@converters @boolean
Feature: Provide tests for the Boolean converter

  Scenario: Should parse the converter
    Given I parse the query:
      """
      eq(a,bool:true)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": true }
        }
      }
      """

  Scenario: Should parse the converter
    Given I parse the query:
      """
      eq(a,bool:1)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": true }
        }
      }
      """

  Scenario: Should parse the converter
    Given I parse the query:
      """
      eq(a,bool:on)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": true }
        }
      }
      """

  Scenario: Should parse the converter
    Given I parse the query:
      """
      eq(a,bool:yes)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": true }
        }
      }
      """

  Scenario: Should parse the converter
    Given I parse the query:
      """
      eq(a,bool:false)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": false }
        }
      }
      """

  Scenario: Should parse the converter
    Given I parse the query:
      """
      eq(a,bool:0)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": false }
        }
      }
      """

  Scenario: Should parse the converter
    Given I parse the query:
      """
      eq(a,bool:off)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": false }
        }
      }
      """

  Scenario: Should parse the converter
    Given I parse the query:
      """
      eq(a,bool:no)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": false }
        }
      }
      """

  Scenario: Should parse the converter
    Given I parse the query:
      """
      eq(a,bool:somethingelse)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": false }
        }
      }
      """
