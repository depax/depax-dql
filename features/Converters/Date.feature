@converters @date
Feature: Provide tests for the Date converter

  Scenario: Should parse the converter
    Given I parse the query:
      """
      eq(a,date:2018-01-23)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": "Tue, 23 Jan 2018 00:00:00 GMT" }
        }
      }
      """

  Scenario: Should parse the converter
    Given I parse the query:
      """
      eq(a,date:2018-01-23T11:27:45.154Z)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": "Tue, 23 Jan 2018 11:27:45 GMT" }
        }
      }
      """

  Scenario: Should parse the converter
    Given I parse the query:
      """
      eq(a,date:1516706865)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": "Tue, 23 Jan 2018 11:27:45 GMT" }
        }
      }
      """

  Scenario: Should parse the converter
    Given I parse the query:
      """
      eq(a,date:hello)
      """
     Then  I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": "Thu, 01 Jan 1970 00:00:00 GMT" }
        }
      }
      """
