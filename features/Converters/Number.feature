@converters @number
Feature: Provide tests for the Number converter

  Scenario: Should parse the converter
    Given I parse the query:
      """
      eq(a,number:1)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": 1 }
        }
      }
      """

  Scenario: Should parse the converter
    Given I parse the query:
      """
      eq(a,number:01859)
      """
    Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": 1859 }
        }
      }
      """

  Scenario: Should parse the converter
    Given I parse the query:
      """
      eq(a,number:hello)
      """
    Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": 0 }
        }
      }
      """
