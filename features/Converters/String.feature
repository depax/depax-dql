@rql @converters @string
Feature: Provide tests for the String converter

  Scenario: Should parse the converter
    Given I parse the query:
      """
      eq(a,string:hello)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": "hello" }
        }
      }
      """

Scenario: Should parse the converter
    Given I parse the query:
      """
      eq(a,string:01859)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": "01859" }
        }
      }
      """
