@rql @converters @regex
Feature: Provide tests for the Regex converter

  Scenario: Should parse the converter
    Given I parse the query:
      """
      eq(a,re:%5Ehell)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": "/^hell/" }
        }
      }
      """

  Scenario: Should parse the converter
    Given I parse the query:
      """
      eq(a,regex:%5Ehell)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": "/^hell/" }
        }
      }
      """

  Scenario: Should parse the converter
    Given I parse the query:
      """
      eq(a,rei:%5Ehell)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": "/^hell/i" }
        }
      }
      """

  Scenario: Should parse the converter
    Given I parse the query:
      """
      eq(a,regexi:%5Ehell)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": "/^hell/i" }
        }
      }
      """
