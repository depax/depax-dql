@converters @auto
Feature: Provide tests for the Auto converters

  Scenario: Should parse the number converter
    Given I parse the query:
      """
      eq(a,99)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": 99 }
        }
      }
      """

  Scenario: Should parse the string converter
    Given I parse the query:
      """
      eq(a,hello)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": "hello" }
        }
      }
      """

  Scenario: Should parse the true converter
    Given I parse the query:
      """
      eq(a,true)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": true }
        }
      }
      """

  Scenario: Should parse the false converter
    Given I parse the query:
      """
      eq(a,false)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": false }
        }
      }
      """

  Scenario: Should parse the null converter
    Given I parse the query:
      """
      eq(a,null)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": null }
        }
      }
      """

  Scenario: Should parse the undefined converter
    Given I parse the query:
      """
      eq(a,undefined)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": "@undefined" }
        }
      }
      """

  Scenario: Should parse the Infinity  converter
    Given I parse the query:
      """
      eq(a,Infinity)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": "@Infinity" }
        }
      }
      """

  Scenario: Should parse the -Infinity converter
    Given I parse the query:
      """
      eq(a,-Infinity)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": "@-Infinity" }
        }
      }
      """
