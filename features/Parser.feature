@parser
Feature: Parser

  Scenario: Should parse operators
    Given I parse the query:
      """
      eq(a,b),sort(+a,-b,c),lt(b,c)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": "b" },
          "b": { "$lt": "c" }
        },
        "sort": {
          "a": true,
          "b": false,
          "c": true
        }
      }
      """

  Scenario: Should get an unknown operator error
    Given I parse the query:
      """
      eq(a,b),sort(+a,-b,c),foo(b,c)
      """
     Then I should have the following query response:
      """
      {
        "errors": [
          "The unknown operator \"foo\" has been identified."
        ],
        "selectors": {
          "a": { "$eq": "b" }
        },
        "sort": {
          "a": true,
          "b": false,
          "c": true
        }
      }
      """

  Scenario: Should get an missing parentheses error
    Given I parse the query:
      """
      eq(a,b),sort(+a,-b,c,lt(b,c)
      """
     Then I should have the following query response:
      """
      {
        "errors": [
          "Opening and closing parentheses mismatch."
        ]
      }
      """

  Scenario: Should get an unknown converter error
    Given I parse the query:
      """
      eq(a,foo:b),sort(+a,-b,c),lt(b,c)
      """
     Then I should have the following query response:
      """
      {
        "errors": [
          "The unknown converter \"foo\" has been identified."
        ],
        "selectors": {
          "b": { "$lt": "c" }
        },
        "sort": {
          "a": true,
          "b": false,
          "c": true
        }
      }
      """

  Scenario: Should get an parse even with having invalid operators in logical operators
    Given I parse the query:
      """
      and(eq(a,b),sort(+a,-b,c),lt(b,c))
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "$and": {
            "a": { "$eq": "b" },
            "b": { "$lt": "c" }
          }
        },
        "sort": {
          "a": true,
          "b": false,
          "c": true
        }
      }
      """

  Scenario: Should get multiple errors
    Given I parse the query:
      """
      eq(a,foo:b),sort(+a,-b,c),foo(b,c)
      """
     Then I should have the following query response:
      """
      {
        "errors": [
          "The unknown converter \"foo\" has been identified.",
          "The unknown operator \"foo\" has been identified."
        ],
        "selectors": {},
        "sort": {
          "a": true,
          "b": false,
          "c": true
        }
      }
      """

  Scenario: Should parse mixed case operators
    Given I parse the query:
      """
      Eq(a,b),SoRt(+a,-b,c),LT(b,c)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": "b" },
          "b": { "$lt": "c" }
        },
        "sort": {
          "a": true,
          "b": false,
          "c": true
        }
      }
      """

  Scenario: Should parse mixed case query operators with params
    Given I parse the query with params:
      """
      {
        "query": "$1(a,$2),SoRt(+a,$3,$4),LT(b,$4)",
        "params": [ "eq", "b", "-b", "c" ]
      }
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": "b" },
          "b": { "$lt": "c" }
        },
        "sort": {
          "a": true,
          "b": false,
          "c": true
        }
      }
      """

  Scenario: Should parse slightly more complex queries
    Given I parse the query:
      """
      eq(a,string:value),and(eq(a,b),lt(b,c),or(eq(b,d),eq(c,e))),sort(a)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": "value" },
          "$and": {
            "a": { "$eq": "b" },
            "b": { "$lt": "c" },
            "$or": {
              "b": { "$eq": "d" },
              "c": { "$eq": "e" }
            }
          }
        },
        "sort": { "a": true }
      }
      """

  Scenario: Should parse a complex query
    Given I parse the query:
      """
      from(users),fields(a,b,c),eq(a,c),and(eq(c,d),lt(e,f)),or(gt(e,g),in(f,a,b,c,d)),limit(10),sort(a,-b,+c)
      """
     Then I should have the following query response:
      """
      {
        "from": "users",
        "fields": [ "a", "b", "c" ],
        "limit": { "limit": 10, "offset": 0 },
        "sort": {
          "a": true,
          "b": false,
          "c": true
        },
        "selectors": {
          "a": { "$eq": "c" },
          "$and": {
            "c": { "$eq": "d" },
            "e": { "$lt": "f" }
          },
          "$or": {
            "e": { "$gt": "g" },
            "f": { "$in": [ "a", "b", "c", "d" ] }
          }
        }
      }
      """
