@comparison-operators @gte
Feature: Provide tests for the GTE comparison operator

  Scenario: Should parse the comparison operator
    Given I parse the query:
      """
      gte(a,b)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$gte": "b" }
        }
      }
      """

  Scenario: Should parse the comparison operator and bind param
    Given I parse the query with params:
      """
      {
        "query": "gte(a,$1)",
        "params": [ "b" ]
      }
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$gte": "b" }
        }
      }
      """
