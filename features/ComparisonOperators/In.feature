@comparison-operators @in
Feature: Provide tests for the IN comparison operator

  Scenario: Should parse the comparison operator
    Given I parse the query:
      """
      in(a,b,c,d)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$in": [ "b", "c", "d" ] }
        }
      }
      """

  Scenario: Should parse the comparison operator and bind param
    Given I parse the query with params:
      """
      {
        "query": "in(a,$1)",
        "params": [ [ "b", "c", "d" ] ]
      }
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$in": [ "b", "c", "d" ] }
        }
      }
      """
