@comparison-operators @eq
Feature: Provide tests for the EQ comparison operator

  Scenario: Should parse the comparison operator
    Given I parse the query:
      """
      eq(a,b)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": "b" }
        }
      }
      """

  Scenario: Should parse the comparison operator and bind param
    Given I parse the query with params:
      """
      {
        "query": "eq(a,$1)",
        "params": [ "b" ]
      }
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$eq": "b" }
        }
      }
      """
