@comparison-operators @lte
Feature: Provide tests for the LTE comparison operator

  Scenario: Should parse the comparison operator
    Given I parse the query:
      """
      lte(a,b)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$lte": "b" }
        }
      }
      """

  Scenario: Should parse the comparison operator and bind param
    Given I parse the query with params:
      """
      {
        "query": "lte(a,$1)",
        "params": [ "b" ]
      }
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$lte": "b" }
        }
      }
      """
