@comparison-operators @nin
Feature: Provide tests for the NIN comparison operator

  Scenario: Should parse the comparison operator
    Given I parse the query:
      """
      nin(a,b,c,d)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$nin": [ "b", "c", "d" ] }
        }
      }
      """

  Scenario: Should parse the comparison operator and bind param
    Given I parse the query with params:
      """
      {
        "query": "nin(a,$1)",
        "params": [ [ "b", "c", "d" ] ]
      }
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$nin": [ "b", "c", "d" ] }
        }
      }
      """
