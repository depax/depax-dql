@comparison-operators @gt
Feature: Provide tests for the GT comparison operator

  Scenario: Should parse the comparison operator
    Given I parse the query:
      """
      gt(a,b)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$gt": "b" }
        }
      }
      """

  Scenario: Should parse the comparison operator and bind param
    Given I parse the query with params:
      """
      {
        "query": "gt(a,$1)",
        "params": [ "b" ]
      }
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$gt": "b" }
        }
      }
      """
