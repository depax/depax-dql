@comparison-operators @lt
Feature: Provide tests for the LT comparison operator

  Scenario: Should parse the comparison operator
    Given I parse the query:
      """
      lt(a,b)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$lt": "b" }
        }
      }
      """

  Scenario: Should parse the comparison operator and bind param
    Given I parse the query with params:
      """
      {
        "query": "lt(a,$1)",
        "params": [ "b" ]
      }
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$lt": "b" }
        }
      }
      """
