@comparison-operators @ne
Feature: Provide tests for the NE comparison operator

  Scenario: Should parse the comparison operator
    Given I parse the query:
      """
      ne(a,b)
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$ne": "b" }
        }
      }
      """

  Scenario: Should parse the comparison operator and bind param
    Given I parse the query with params:
      """
      {
        "query": "ne(a,$1)",
        "params": [ "b" ]
      }
      """
     Then I should have the following query response:
      """
      {
        "selectors": {
          "a": { "$ne": "b" }
        }
      }
      """
