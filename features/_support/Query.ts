/**
 * Provides the step definitions for the feature tests.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import chai = require("chai");
import { Given, Then } from "cucumber";
import Parser, { IQuery } from "../../src";

let lastParsedQuery: IQuery;

//#region ----< Define the *Given* steps >------------------------------------------------------------------------------
Given(/^I parse the query:$/, async (
    query: string,
): Promise<void> => {
    lastParsedQuery = Parser(query);
});

Given(/^I parse the query with params:$/, async (
    payload: any,
): Promise<void> => {
    payload = JSON.parse(payload);
    lastParsedQuery = Parser(payload.query, payload.params);
});
//#endregion

// ----< Define the *Then* steps >--------------------------------------------------------------------------------------
// #region
Then(/^I should have the following query response:$/, async (
    payload: any,
): Promise<void> => {
    payload = JSON.parse(payload);

    // A quick hack to convert Date and RegExp objects to a string equivilant for test purposes.
    if (payload.selectors) {
        Object.keys(payload.selectors).forEach((field) => {
            Object.keys(payload.selectors[field]).forEach((op) => {
                if (typeof payload.selectors[field][op] !== "string") {
                    return;
                }

                if (lastParsedQuery.selectors[field][op] instanceof Date) {
                    lastParsedQuery.selectors[field][op] = lastParsedQuery.selectors[field][op].toUTCString();
                } else if (lastParsedQuery.selectors[field][op] instanceof RegExp) {
                    lastParsedQuery.selectors[field][op] = lastParsedQuery.selectors[field][op].toString();
                } else if (payload.selectors[field][op] === "@undefined") {
                    payload.selectors[field][op] = undefined;
                } else if (payload.selectors[field][op] === "@Infinity") {
                    payload.selectors[field][op] = Infinity;
                } else if (payload.selectors[field][op] === "@-Infinity") {
                    payload.selectors[field][op] = -Infinity;
                }
            });
        });
    }

    chai.assert.deepEqual(lastParsedQuery, payload);
});
//#endregion
