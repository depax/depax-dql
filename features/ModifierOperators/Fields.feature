@modifier-operators @fields
Feature: Provide tests for the FIELDS modifier operator

  Scenario: Should parse the modifier operator
    Given I parse the query:
      """
      fields(a,b,c)
      """
     Then I should have the following query response:
      """
      {
        "fields": [ "a", "b", "c" ]
      }
      """

  Scenario: Should parse the modifier operator and bind param
    Given I parse the query with params:
      """
      {
        "query": "fields(a,$1,c)",
        "params": [ "b" ]
      }
      """
     Then I should have the following query response:
      """
      {
        "fields": [ "a", "b", "c" ]
      }
      """
