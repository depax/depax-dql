@modifier-operators @from
Feature: Provide tests for the FROM modifier operator

  Scenario: Should parse the modifier operator
    Given I parse the query:
      """
      from(users)
      """
     Then I should have the following query response:
      """
      {
        "from": "users"
      }
      """

  Scenario: Should parse the modifier operator and bind param
    Given I parse the query with params:
      """
      {
        "query": "from($1)",
        "params": [ "users" ]
      }
      """
     Then I should have the following query response:
      """
      {
        "from": "users"
      }
      """
