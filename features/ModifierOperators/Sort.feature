@modifier-operators @sort
Feature: Provide tests for the SORT modifier operator

  Scenario: Should parse the modifier operator
    Given I parse the query:
      """
      sort(+a,-b,c)
      """
     Then I should have the following query response:
      """
      {
        "sort": {
          "a": true,
          "b": false,
          "c": true
        }
      }
      """

  Scenario: Should parse the modifier operator and bind param
    Given I parse the query with params:
      """
      {
        "query": "sort(+a,$1,c)",
        "params": [ "-b" ]
      }
      """
     Then I should have the following query response:
      """
      {
        "sort": {
          "a": true,
          "b": false,
          "c": true
        }
      }
      """
