@modifier-operators @limit
Feature: Provide tests for the LIMIT modifier operator

  Scenario: Should parse the modifier operator
    Given I parse the query:
      """
      limit(25,0)
      """
     Then I should have the following query response:
      """
      {
        "limit": { "limit": 25, "offset": 0 }
      }
      """

  Scenario: Should parse the modifier operator with just count
    Given I parse the query:
      """
      limit(25)
      """
     Then I should have the following query response:
      """
      {
        "limit": { "limit": 25, "offset": 0 }
      }
      """

  Scenario: Should parse the modifier operator and bind param
    Given I parse the query with params:
      """
      {
        "query": "limit($1,0)",
        "params": [ 25 ]
      }
      """
     Then I should have the following query response:
      """
      {
        "limit": { "limit": 25, "offset": 0 }
      }
      """
