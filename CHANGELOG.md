# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.2] - 2018-10-24
### Updated

- Amended package script to use `prepublishOnly` instead of `prepare`.

## [1.0.1] - 2018-10-22
### Updated

- Updated the README with badges from the CircleCI builds.
- Updated the package.json file to include publish scirpt and license set to Unlicense

### Added

- Included the [unlicense] license details.
- Added this CHANGELOG file.
- Implemented submodule into the develop branch to handle the builds.
- Added to CircleCI.

## [1.0.0] - 2018-10-04

Initial work done.

[unlicense]: http://unlicense.org/
[1.0.0]: https://bitbucket.org/depax/depax-dql/src/v1.0.0/
[1.0.1]: https://bitbucket.org/depax/depax-dql/src/v1.0.1/
[1.0.2]: https://bitbucket.org/depax/depax-dql/src/v1.0.1/
